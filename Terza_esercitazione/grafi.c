#include <stdio.h>
#include<stdlib.h>

#define MAX_GRAPH 1000
#define CARDINALITY 20

struct nodo
{
  int vertice;
  int weight;
  struct nodo *next;
};

typedef struct nodo Nodo;

struct graph{
  Nodo* adj[MAX_GRAPH];
  int p[MAX_GRAPH];
  int cardinality;
};
typedef struct graph graph;




void inizializzaGrafo(graph *G)
{
  G->cardinality=CARDINALITY;
  for(int i=0;i<MAX_GRAPH;i++)
  {
    G->adj[i]=NULL;
    G->p[i]=-1;
  }
}

void PrintGrafo(graph *G,int numero_grafi)
{
  for (int i=0;i<G->cardinality;i++)
  {
    printf("%d --> ",i);
    Nodo *current;
    current=G->adj[i];
    while (current)
    {
      printf("%d ",current->vertice);
      current=current->next;
    }
    printf("\n");
  }
  printf("***************************************************\n");

}

void addEdje(graph *G,int i,int j,int w )
{
  Nodo* newNode;
  newNode=(Nodo*)malloc(sizeof(Nodo));
  newNode->weight=w;
  newNode->vertice=j;
  if (G->adj[i] != NULL)
  {
    Nodo* aux;
    aux= G->adj[i];
    G->adj[i]=newNode;
    newNode->next=aux;
  }
  else
    G->adj[i]=newNode;
}

void generaGrafo( graph *G, int numero_grafi)
{
  int p,i,j;
  for(p=1;p <= numero_grafi; p++)
  {
    inizializzaGrafo(G);
    printf("GRAFO %d\n",p);
    for (i=0; i<G->cardinality;i++)
    {
      for(j=i+1;j<G->cardinality;j++)
      {
        if (rand()%numero_grafi < p )
        {
          int w=rand()%100;
          addEdje(G,i,j,w);
          addEdje(G,j,i,w);
        }
      }
    }
    PrintGrafo(G,numero_grafi);
    //printf("\n\n");
  }
}


int main()
{
  graph G;
  //inizializzaGrafo(&G);

  int numero_grafi;
  printf("quanti grafi vuoi generare? ");
  scanf("%d",&numero_grafi);
  
  
  generaGrafo(&G,numero_grafi);
}
