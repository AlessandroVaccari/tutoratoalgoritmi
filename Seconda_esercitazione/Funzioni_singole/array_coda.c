#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <math.h>

#define MAX_CODA 100001                // nelle nostre code ci sono sempre al massimo MAX_CODA elementi
#define NUM_ESPERIMENTI 200            // che si chiamano 0,1,...,MAX_CODA-1
#define SALTO 100
#define MAX_PRIORITA 1000


// ************************ CODE SU ARRAY ******* //

struct elemento_coda_array{
    int priorita;
    int empty;
};

typedef struct elemento_coda_array elemento_coda_array;

struct tipo_coda_array{
    elemento_coda_array coda[MAX_CODA];            // gli elementi che possono entrare nella si chiamano tutti da 0 a MAX_CODA-1
    int size;
};

typedef struct tipo_coda_array coda_array;

void stampa_coda_array(coda_array q)
{
    for (int i=0; i<MAX_CODA; i++)
        if (q.coda[i].empty==0) printf("sig. %d con priorita %d\n",i,q.coda[i].priorita);
    printf("\n");
}

void inizializza_coda_array(coda_array *q,int num_elementi)
{
    q->size=num_elementi;                       // come detto, in questo particolare esperimento la dimensione è fissa
    for (int i=0; i<num_elementi; i++)
    {
        q->coda[i].empty=0;
        q->coda[i].priorita=rand()%MAX_PRIORITA;
    }
}

int enqueue_coda_array(coda_array *q,int nome,int priorita)
{
  if(nome>=MAX_CODA)
    printf("OVERFLOW\n");
  else
    {
      q->coda[nome].priorita=priorita;
    }
}

int extract_min_coda_array(coda_array *q,int num_elementi)
{
  int minIndex=-1;
  int minPriority=INT_MAX;
  for(int i=0;i<num_elementi;i++)
  {
    if (q->coda[i].priorita < minPriority && !q->coda[i].empty)
    {
      minPriority=q->coda[i].priorita;
      minIndex=i;
    }
  }
  if (minIndex==-1)
    printf("UNDERFLOW\n");
  else
  {
    q->coda[minIndex].empty=1;
    return minIndex;
  }
}

void printProgressBar(double percentage) {
	printf("[");
	int PBWIDTH = 70;
	int pos = PBWIDTH * percentage;
	int value = PBWIDTH - (int)(percentage*PBWIDTH);
	for (int i = 0; i < PBWIDTH; ++i) {
		if (i < pos)
			printf("=");
		else if (i == pos)
			printf(">");
		else
			printf(" ");
	}
	printf("] %d %%\r", (100 - value));
	fflush(stdout);
}


int main()
{
    int seed;
    FILE *fp;
	int indice_esperimento;
	int indice_ripetizione;
	int num_operazioni;
	clock_t inizio_h,fine_h;
    clock_t inizio_a,fine_a;

                                     // riempiamo questa heap
    coda_array q;                               // e questa coda

    int temp_elem_coda_array;

	// per rendere l'esperimento ripetibile
	printf("Inserisci seme: ");
	scanf("%d", &seed);
	srand(seed);

	fp = fopen("tempi_code.txt", "w");
	printf("Inizio Esperimenti\n");

    num_operazioni=10;

		printf("\nFaccio %d operazioni\n",num_operazioni);

		inizializza_coda_array(&q,num_operazioni);
    for(int j=0;j<num_operazioni;j++)
    {
      printf("%d ",q.coda[j].priorita);

    }printf("\n");
		//*******************************CODA***************************
    num_operazioni=3;
		for (int num_extract=0; num_extract<num_operazioni; num_extract++)
        {
            temp_elem_coda_array=extract_min_coda_array(&q,10);
            printf("\n\n%d\n\n",temp_elem_coda_array);
            enqueue_coda_array(&q,temp_elem_coda_array,rand()%MAX_PRIORITA);
            for(int j=0;j<10;j++)
            {
              printf("%d ",q.coda[j].priorita);

            }printf("\n");
        }
        fine_a = clock();
        fprintf(fp, "%d\t%d\t%d\n", num_operazioni, fine_h-inizio_h, fine_a-inizio_a);
    printf("Fine Esperimenti\n");
	fclose(fp);
    return 0;
}
