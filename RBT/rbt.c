#include "rbt.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
// #include<malloc.h>

//--->  START FUNCTION RED-BLACK TREE___________________________________________________________________________//

/*
##############################
#   COLOR:                   #
#       -   1 --> RED        #
#       -   0 --> BLACK      #
############################## */


//--->  START INIZIALIZATION_________________________________________________________//
void inizializationRB(RbTree *T)
{
    T->Nil=NULL;
    T->root=NULL;
}
//--->  END INIZIALIZATION___________________________________________________________//

//--->  START LEFT ROTATE____________________________________________________________//
void leftRotate( RbTree *T, RbTreeNode *current)
{
    RbTreeNode *y=current->right;
    current->right=y->left;
    if(y->left!=T->Nil)
        y->left->parent=current;

    y->parent=current->parent;

    if(current->parent == T->Nil)
        T->root=y;
    else
        if( current->parent != T->Nil && current==current->parent->left)
            current->parent->left=y;
        else
            if( current->parent != T->Nil && current==current->parent->right)
                current->parent->right=y;
    y->left = current;
    current->parent=y;
}
//--->  END LEFT ROTATE______________________________________________________________//

//--->  START RIGHT ROTATE___________________________________________________________//
void rightRotate( RbTree *T, RbTreeNode *current)
{
    RbTreeNode *y=current->left;
    current->left=y->right;
    if(y->right !=T->Nil)
        y->right->parent=current;
    y->parent=current->parent;
    if(current->parent==T->Nil)
        T->root=y;
    if (current-> parent != T->Nil && current==current->parent->right)
        current->parent->right=y;
    else
        if(current-> parent != T->Nil && current==current->parent->left)
            current->parent->left=y;
    y->right=current;
    current->parent=y;
}
//--->  END RIGHT ROTATE_____________________________________________________________//

//-->   START TREE INSERT FIXUP LEFT_________________________________________________//
RbTreeNode* TreeInsertFixupRBleft(RbTree *T, RbTreeNode *node)
{
    RbTreeNode *current=node;
    RbTreeNode *y;
    y=current->parent->parent->right;
    if(y!=T->Nil && y->color ==  1)
    {
        current->parent->color=0;
        y->color=0;
        current->parent->parent->color=1;
        current=current->parent->parent;
    }
    else
    {
        if(current==current->parent->right)
        {
            current=current->parent;
            leftRotate(T,current);
        }
        current->parent->color=0;
        current->parent->parent->color=1;
        rightRotate(T,current->parent->parent);
    }
    return current;
}
//-->   END TREE INSERT FIXUP LEFT___________________________________________________//

//-->   START TREE INSERT FIXUP RIGHT________________________________________________//
RbTreeNode* TreeInsertFixupRBright(RbTree *T, RbTreeNode *current)
{
    RbTreeNode *y;
    y=current->parent->parent->left;
    if(y!=T->Nil && y->color ==  1)
    {
        current->parent->color=0;
        y->color=0;
        current->parent->parent->color=1;
        current=current->parent->parent;
    }
    else
    {
        if(current==current->parent->left)
        {
            current=current->parent;
            rightRotate(T,current);
        }
        current->parent->color=0;
        current->parent->parent->color=1;
        leftRotate(T,current->parent->parent);
    }
    return current;
}
//-->   END TREE INSERT FIXUP RIGHT__________________________________________________//

//-->   START TREE INSERT FIXUP______________________________________________________//
void TreeInsertFixupRB(RbTree *T,RbTreeNode *NewNode)
{
    RbTreeNode* current=NewNode;
    while (current->parent != T->Nil && current->parent->color == 1)
        if(current->parent == current->parent->parent->left)
             current=TreeInsertFixupRBleft(T,current);
        else
            current=TreeInsertFixupRBright(T,current);
    T->root->color=0;
}
//-->   END TREE INSERT FIXUP________________________________________________________//

//-->   START TREE INSERT____________________________________________________________//
void TreeInsertRB(RbTree *T, int key,/*VERSIONE PROGETTO -->*/int* MaxRip)
{

    RbTreeNode* newNode;
    newNode=(RbTreeNode*)malloc(sizeof(RbTreeNode));
    newNode->key=key;

    RbTreeNode* y;
    RbTreeNode* x;

    x=T->root;
    y=T->Nil;

    int trovato=0;
    while( x != T->Nil && !trovato)
    {
      /* VERSIONE PROGETTO*/
        if (x->key == newNode->key)
        {
            x->ripetizioni++;
            if(x->ripetizioni > (*MaxRip) )
                (*MaxRip)++;
            trovato=1;
        }
        else
        {
            y=x;
            if (newNode->key < x->key)
                x=x->left;
            else
                x=x->right;
        }

    }
    if(!trovato)
    {
        newNode->parent=y;
        if (y==T->Nil)
        {
            T->root=newNode;
            T->root->parent=T->Nil;
            T->root->ripetizioni=1;
        }
        else
        {
            if(y != T->Nil && newNode->key < y->key )
            {
                y->left=newNode;
                y->left->ripetizioni=1;
            }
            else
                if(y != T->Nil && newNode->key > y->key )
                {
                    y->right=newNode;
                    y->right->ripetizioni=1;
                }
        }
        newNode->left=T->Nil;
        newNode->right=T->Nil;
        newNode->color=1;
        TreeInsertFixupRB(T,newNode);
    }
}
//-->   END TREE INSERT______________________________________________________________//


//-->   START TREE FREE NODE_________________________________________________________//
void TreeFreeNodeRB(RbTreeNode *current)
{                                   // Non elimina T->NIl;
    if (current != NULL) {
      TreeFreeNodeRB(current->left);
      TreeFreeNodeRB(current->right);
      free(current);
    }
}
//-->   END TREE FREE NODE___________________________________________________________//

//--->  END FUNCTION RED_BLACK TREE_____________________________________________________________________________//
