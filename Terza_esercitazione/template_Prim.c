﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>

#define MAXVERTEX 4001       //numero massimo di vertici del grafo
#define MAXQUEUE  MAXVERTEX  //dimensione coda = MAXnome
#define MAXWEIGHT 10         //massimo peso arco
#define SALTO 50

/**************************************** GRAFI */

struct VertexListElement{
    int Name;
    int EdgeWeight;
    struct VertexListElement* Next;
};

typedef struct VertexListElement VertexListElement;

struct Vertex {
    int Key;                          //used for Prim only
    int Parent;                       //used for Prim only
    VertexListElement* AdjHead;       //pointer to the head of the adjacency list
};

typedef struct Vertex Vertex;

struct Graph {                        // a graph
	Vertex VertexArray[MAXVERTEX];
	int Cardinality;
};
typedef struct Graph Graph;

void PrintAdjList(VertexListElement* Head)
{
    if (Head==NULL) return;
    while (Head)
    {
        printf("%d (peso %d)",Head->Name, Head->EdgeWeight);
        Head=Head->Next;
    }
}

void PrintGraphStandard(Graph *G)
{
	printf("\nGraph has %d vertexes \n", G->Cardinality);
	for (int i = 0; i<G->Cardinality; i++)
    {
		printf("Vertex %d connected to: ", i);
		if (G->VertexArray[i].AdjHead) PrintAdjList(G->VertexArray[i].AdjHead);
		printf("\n");
	}
}

void PrintGraphAfterPrim(Graph *G)
{
	printf("\nGraph has %d vertexes \n", G->Cardinality);
	for (int i = 0; i<G->Cardinality; i++)
    {
		printf("Vertex %d has rank %d and parent %d: ",i,G->VertexArray[i].Key,G->VertexArray[i].Parent);
		printf("\n");
	}
}


void InsertVertexAdj(Graph *G,int Vertex1,int Vertex2,int Weight)
{
    VertexListElement* temp=malloc(sizeof(VertexListElement));
    temp->Name=Vertex2;
    temp->Next=NULL;
    temp->EdgeWeight=Weight;
    if (G->VertexArray[Vertex1].AdjHead==NULL)
    {
        G->VertexArray[Vertex1].AdjHead=temp;
        return;
    }
    temp->Next=G->VertexArray[Vertex1].AdjHead;
    G->VertexArray[Vertex1].AdjHead=temp;
    return;
}

void InitializeRandomGraph(Graph *G,int NumVertexes,int ProbEdge)
{
    G->Cardinality=NumVertexes;
    for (int i=0; i<NumVertexes; i++)
    {
        for (int j=i+1; j<NumVertexes; j++)
        {
            if (1)                                   // ATTENZIONE QUI: ADESSO COSTRUISCE SOLO GRAFI COMPLETI. COME FACCIAMO A RENDERLO GENERICO?
            {                                        // QUAL È IL RUOLO DI 'PROBEDGE'?
                int Weight=rand()%MAXWEIGHT;
                InsertVertexAdj(G,i,j,Weight);
                InsertVertexAdj(G,j,i,Weight);
            }
        }
    }
}

void FreeList(VertexListElement* Head)
{
   if (Head->Next) FreeList(Head->Next);
   free(Head);
   Head=NULL;
}

void FreeGraph(Graph *G)
{
	for (int i = 0; i<G->Cardinality; i++)
    {
        if (G->VertexArray[i].AdjHead)
        {
             FreeList(G->VertexArray[i].AdjHead);
             G->VertexArray[i].AdjHead=NULL;
        }
	}
}

/**************************************** HEAPS */

struct HeapElement {
	int Name;
	int Priority;
};

typedef struct HeapElement HeapElement;

struct MinHeap {
	int Positions[MAXVERTEX];
	HeapElement Heap[MAXVERTEX];
	int HeapSize;
};
typedef struct MinHeap MinHeap;

int Left(int i)
{
	return 2 * i + 1;
}

int Right(int i)
{
	return 2 * i + 2;
}

int Parent(int i)
{
	if (i == 0)
		return 0;
	if ((i % 2) == 0)
		return i / 2 - 1;
	return i / 2;
}

void PrintMinHeap(MinHeap* H) {

	for (int i = 0; i<H->HeapSize; i++)
		printf("Vertice %d con chiave %d in posizione %d\n", H->Heap[i].Name, H->Heap[i].Priority, H->Positions[H->Heap[i].Name]);
    printf("\n");
}


void DecreaseKey(MinHeap *H,int Vertex, int NewKey)
{
	                                // COMPLETARE QUESTA FUNZIONE IN MANIERA SIMILE ALL'ESERCIZIO 2
}


void MinHeapify(MinHeap *H, int i)
{
	int l = Left(i);
	int r = Right(i);
	int smallest = i;
	HeapElement temp;

	if ((l <= H->HeapSize - 1) && (H->Heap[l].Priority < H->Heap[smallest].Priority))
		smallest = l;
	if ((r <= H->HeapSize - 1) && (H->Heap[r].Priority < H->Heap[smallest].Priority))
		smallest = r;
	if (smallest == i)
		return;
	H->Positions[H->Heap[i].Name] = smallest;
	H->Positions[H->Heap[smallest].Name] = i;
	temp = H->Heap[smallest];
	H->Heap[smallest] = H->Heap[i];
	H->Heap[i] = temp;
	MinHeapify(H,smallest);
	return;
}

void BuildMinHeap(MinHeap *H) {
	for (int i = (((H->HeapSize) - 1) / 2); i >= 0; i--)
		MinHeapify(H, i);
}

HeapElement ExtractMin(MinHeap *H)
{
	                                // COMPLETARE QUESTA FUNZIONE IN MANIERA SIMILE ALL'ESERCIZIO 2
									// ATTENZIONE: COSA SUCCEDE DELL'ELEMENTO ELIMINATO? COME USIAMO QUESTA INFO NELL'ALGORITMO DI PRIM?

}


/**************************************** ARRAYS */

struct CodaArray
{
	int priority[MAXQUEUE];
	int empty[MAXQUEUE];
	int length;
};
typedef struct CodaArray CodaArray;

void StampaArray(CodaArray *Q)
{
	int i = 0;
	int stampati = 0;

	while (stampati < Q->length) {
		if (Q->empty[i] == 0) {
			printf("Indice: %d, priorita: %d, empty: %d\n", i, Q->priority[i], Q->empty[i]);
			stampati++;
		}
		i++;
	}

	printf("\n");
}


void InizializzaArray(CodaArray *Q, int cardinalita)
{
	int i;

	for (i = 0; i < cardinalita; i++) {
		Q->empty[i] = 0;
	}

	for (i = cardinalita; i < MAXQUEUE; i++) {
		Q->empty[i] = 1;
	}

	Q->length = 0;
}

int ExtractMinArray(CodaArray *Q)
{
	                                // COMPLETARE QUESTA FUNZIONE IN MANIERA SIMILE ALL'ESERCIZIO 2
									// ATTENZIONE: COSA SUCCEDE DELL'ELEMENTO ELIMINATO? COME USIAMO QUESTA INFO NELL'ALGORITMO DI PRIM?

}

int IsCodaArrayEmpty(CodaArray *Q)
{
	if (Q->length == 0)
		return 1;
	else
		return 0;
}

void EnqueueArray(CodaArray *Q, int priority)
{
	                                // COMPLETARE QUESTA FUNZIONE IN MANIERA SIMILE ALL'ESERCIZIO 2
									
}



void MSTPrimArray(Graph *G, int Source)
{
}



void MstPrim(Graph *G,int Source)
{

}

int ComputeMstWeigth(Graph *G)
{
    int weight=0;
    for (int i=0; i<G->Cardinality; i++)
    {
        weight=weight+G->VertexArray[i].Key;
    }
    return weight;
}

int main()
{

	FILE *fp;
	int seed;
	clock_t inizio_h,fine_h,inizio_a,fine_a;
	fp = fopen("tempi.txt", "w");
	if (fp == NULL)
    {
		perror("Errore apertura file");
		exit(EXIT_FAILURE);
	}

	printf("Inserisci seme: ");
	scanf("%d", &seed);
	srand(seed);
	printf("Inizio Esperimenti\n");

    Graph G;
    for (int i=1; i<100; i++)
    {
        InitializeRandomGraph(&G,3000,i);
        inizio_h=clock();
        MstPrim(&G,0);
        fine_h=clock();
        inizio_a=clock();
        MSTPrimArray(&G,0);
        fine_a=clock();
        FreeGraph(&G);
        printf("\nFatto %d",i);
        fprintf(fp,"%d\t%d\t%d\n",i,fine_h-inizio_h,fine_a-inizio_a);
    }

    close(fp);
    return 0;
}
