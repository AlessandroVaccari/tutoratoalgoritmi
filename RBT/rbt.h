struct RbTreeNode{
    int key;
    struct RbTreeNode *parent;
    struct RbTreeNode *left;
    struct RbTreeNode *right;
    int color;
    int ripetizioni;
};
typedef struct RbTreeNode RbTreeNode;

struct RbTree{
    RbTreeNode *root;
    RbTreeNode *Nil;
    int cardinality;
};
typedef struct RbTree RbTree;

RbTreeNode* TreeInsertFixupRBleft(RbTree *T, RbTreeNode *newNode);
RbTreeNode* TreeInsertFixupRBright(RbTree *T, RbTreeNode *newNode);
void TreeInsertRB(RbTree *T,int key,/*VERSIONE PROGETTO -->*/int* MaxRip);
void TreePrintRB( RbTreeNode *current, RbTreeNode *Nil );
void inizializationRB(RbTree *T);
void TreeFreeNodeRB(RbTreeNode *current);
