#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<limits.h>

#define DIMENSIONE 10000
#define RIPETIZIONI 4000
#define SALTO 10


void Scambia(int *c,int *b)
{
    int temp;
    temp=*c;
    *c=*b;
    *b=temp;
}
/* QUICK SORT */

/*suddivisione array*/
int Standard_Partition(int *a,int left_index,int dimensione)
{
    int x=a[dimensione-1];	//elemento di confronto
    int i = left_index-1;		//decremento i come da pseudo-codice
    for(int j=left_index;j<dimensione-1;j++) //scorro fino a n-1 perchè n-esimo elemento è di confronto
    {
        if (a[j]<=x)		//se elemento j-esimo è minore lo scambia, ma se è già ordinato lo scambia con se stesso
        {
            i++;
            Scambia(&a[i],&a[j]);
        }
    }
    i++;
    Scambia(&a[i],&a[dimensione-1]); //metto elemento di confronto a separare i due insiemi
return i;
}

/*quick sort base che usa standardPartition*/
void Standard_Quick_Sort(int *a,int left_index,int dimensione)
{
    if(left_index<dimensione)		// lo faccio fino a quando non ho insieme unitario
    {
        int compare_index=Standard_Partition(a,left_index,dimensione); //	mi ritorna indice (in A[]) dell'elemento di comparazione
        Standard_Quick_Sort(a,left_index,compare_index);	// Applico Standard_Quick_Sort su insieme sx
        Standard_Quick_Sort(a,compare_index+1,dimensione);	// Applico Standard_Quick_Sort su insieme dx
    }
}
 
/* QUICK SORT */

/*suddivisione array*/
int Random_Partition(int *a,int left_index,int dimensione)
{
    int l=rand()%(dimensione-left_index) +left_index;
    int x=a[l];	//elemento di confronto
	Scambia(&a[l],&a[dimensione-1]);
    int i = left_index-1;		//decremento i come da pseudo-codice
    for(int j=left_index;j<dimensione-1;j++) //scorro fino a n-1 perchè n-esimo elemento è di confronto
    {
        if (a[j]<=x)		//se elemento j-esimo è minore lo scambia, ma se è già ordinato lo scambia con se stesso
        {
            i++;
            Scambia(&a[i],&a[j]);
        }
    }
    i++;
    Scambia(&a[i],&a[dimensione-1]); //metto elemento di confronto a separare i due insiemi
return i;
}

/*quick sort base che usa RandomPartition*/
void Random_Quick_Sort(int *a,int left_index,int dimensione)
{
    if(left_index<dimensione)		// lo faccio fino a quando non ho insieme unitario
    {
        int compare_index=Random_Partition(a,left_index,dimensione); //	mi ritorna indice (in A[]) dell'elemento di comparazione
        Random_Quick_Sort(a,left_index,compare_index);	// Applico Random_Quick_Sort su insieme sx
        Random_Quick_Sort(a,compare_index+1,dimensione);	// Applico Random_Quick_Sort su insieme dx
    }
}

/*soluzione con int max*/
void Merge_Int_Max(int *A, int left_index, int middle_index, int right_index) {
    int n1=middle_index - left_index +1;  //Dimensione array di sinistra
    int n2=right_index - middle_index;    //Dimensione array di destra
    int L[n1+1],R[n2+1];                  //Dichiaro array e aggiungo spazio per real_max
    int i,j=left_index;
    for (i=0; i<n1 ; i++){                //riempio array di sinistra con elementi a partire da left_index(uguale a 0solo al primo giro)
        L[i]=A[j];
        j++;
    }
    for (i=0; i<n2 ; i++){                //riempio secondo array a partire dalla j-esima posizione di A[]
        R[i]=A[j];
        j++;
    }
    L[n1]= INT_MAX;                       //Ultimi elementi dei due array eguagliati a real_max
    R[n2]= INT_MAX;

    i=0;j=0;
    int k;
    for(k=left_index;k<=right_index;k++)   //Riempimento ordinato di A[] in base al confronto dell'i-esimo con il j-esimo elemento
    {                                      // se inserisco i prossimo confronto con i+1-esimo elemento con j e viceversa
        if(L[i]<=R[j])
        {
            A[k]=L[i];
            i++;
        }
        else
        {
            A[k]=R[j];
            j++;
        }
    }
}

  void Merge_Int_Max_Sort(int *A, int left_index, int right_index) {
      if (left_index < right_index) //rompo array fino ad arrivare a casella unitaria
      {
          int middle_index= (left_index + right_index) / 2 ;
          Merge_Int_Max_Sort(A,left_index,middle_index);  //ordinamento su nuova parte dx
          Merge_Int_Max_Sort(A,middle_index+1 , right_index); //ordinamento su nuova parte sx
          Merge_Int_Max(A,left_index,middle_index,right_index); //inserimento ordinato delle due parti relative
                                                                //alla n-esima chiamata ricorsiva
      }
  }


void CreaArray(int *A,int *B, int *C)
{
    int j=DIMENSIONE;
    for (int i=0;i<DIMENSIONE;i++)
    {
        A[i]=j;
        B[i]=j;
        C[i]=j;
        j--;
    }
}
void Stampa(int *A)
{
    for(int i=0;i<DIMENSIONE;i++)
    {
        printf("%d ",A[i]);
    }
}
void Disordina(int *A, int *B, int *C,int limit)
{
    for(int i=0;i<limit; i++)
    {
        int x=rand()%DIMENSIONE;
        int y=rand()%DIMENSIONE;

        Scambia(&A[x],&A[y]);
    
        Scambia(&B[x],&B[y]);

        Scambia(&C[x],&C[y]);
    }
}

int main()
{
    int S[DIMENSIONE];
    int R[DIMENSIONE];
    int M[DIMENSIONE];
    clock_t inizio,fine,standard,random,merge;
    FILE *fp;
    fp=fopen("Tempi.txt","w");
   
    for(int i=0;i<=RIPETIZIONI;i+=SALTO)
    {
        CreaArray(S,R,M);
        Disordina(S,R,M,i);
        inizio=clock();
        Standard_Quick_Sort(S,0,DIMENSIONE);
        fine=clock();
        standard=fine-inizio;
        
        inizio=clock();
        Random_Quick_Sort(R,0,DIMENSIONE);
        fine=clock();
        random=fine-inizio;

        inizio=clock();
        Merge_Int_Max_Sort(M,0,DIMENSIONE-1);
        fine=clock();
        merge=fine-inizio;


        fprintf(fp,"%d\t%ld\t%ld\t%ld\n",i,standard,random,merge);
    }
    /*
    inizio=clock();
    Random_Quick_Sort(R,0,DIMENSIONE);
    fine=clock();
    random=fine-inizio;
    printf("\ntempo= %ld\n",random);
    */
}